#include <iostream>
#include <cstdio>
using namespace std;

int main() {

    int a, b, i;
    cin >> a;
    cin >> b;

    for (i = a; i <= b; i++)
    {
        if (i >= 1 || i <= 9)  //* Prints the number entered from 1 to 9 on the screen.
        {
            if (i == 1)
            {
                cout << "one" << endl;
            }
            else if (i == 2)
            {
                cout << "two" << endl;
            }
            else if (i == 3)
            {
                cout << "three" << endl;
            }
            else if (i == 4)
            {
                cout << "four" << endl;
            }
            else if (i == 5)
            {
                cout << "five" << endl;
            }
            else if (i == 6)
            {
                cout << "six" << endl;
            }
            else if (i == 7)
            {
                cout << "seven" << endl;
            }
            else if (i == 8)
            {
                cout << "eight" << endl;
            }
            else if (i == 9)
            {
                cout << "nine" << endl;
            }
        }
        if (i > 9)
        {
            if (i % 2 == 0)  //* Checks whether the entered Number is an even count or an odd number.
            {
                cout << "even" << endl;  //* If the number is even, it writes "even" to the screen
            }
            else
            {
                cout << "odd" << endl;  //* If the number is odd, it writes "odd" to the screen
            }
        }

    }

    return 0;
}
