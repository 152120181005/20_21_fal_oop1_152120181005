#include <stdio.h>
#include<iostream>
using namespace std;

void update(int* a, int* b) {
    int A = *a;
    *a = *a + *b;
    *b = A - *b;
    if (*b < 0)
    {
        *b *= -1;  //* If the number inside the absolute value is negative, it exits by multiplying by -1.
    }
}

int main() {
    int a, b;
    int* pa = &a, * pb = &b;

    cin >> a;
    cin >> b;

    update(pa, pb);

    cout << a << endl;
    cout << b<<endl;

    return 0;
}