#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

/// This function sums the numbers from 3 integer types requested by the user and prints it to the screen
int main() {

    int a, b, c;
    cin >> a >> b >> c;
    cout << a + b + c;

    return 0;
}