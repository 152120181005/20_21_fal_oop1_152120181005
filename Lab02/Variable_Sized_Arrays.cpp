#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#define size 1000
using namespace std;

int main() {

    int n, q, A[size][size], S, x1, x2;
    cin >> n >> q;
    for (int i = 0; i < n; i++)
    {
        cin >> S; // Size of array
        for (int j = 0; j < S; j++)
        {
            cin >> A[i][j];
        }
    }

    for (int k = 0; k < q; k++)
    {
        cin >> x1 >> x2;
        cout << A[x1][x2] << endl;
    }

    return 0;
}
