#include <iostream>
#include <cstdio>
using namespace std;

int max_of_four(int, int, int, int);

int main() {
    int a, b, c, d;
    cin >> a;
    cin >> b;
    cin >> c;
    cin >> d;
    int ans;
    ans = max_of_four(a, b, c, d);
    cout << ans;

    return 0;
}

int max_of_four(int a, int b, int c, int d) //* This function returns the largest of the 4 numbers entered
{
    int max, M[4] = { a,b,c,d };

    max = M[0];
    for (int i = 0; i < 4; i++)
    {
        if (max < M[i])
        {
            max = M[i];
        }
    }

    return max;
}
