#include <iostream>
#include <cstdio>
#include <iomanip>
using namespace std;

int main() {

    int a;
    long int b;
    char c;
    float d;
    double e;

    cin >> a >> b >> c >> d >> e;
    cout << a << endl;
    cout << b << endl;
    cout << c << endl;
    cout << setprecision(7) << d << endl;
    cout << setprecision(10) << e;

    return 0;
}
