#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {

    int n, A[1000], B[1000];
    cin >> n;
    int j = 0, x = n - 1;

    //* This function suppresses the sequence of numbers entered in the desired dimension in reverse.

    for (int i = 0; i < n; i++)
    {
        cin >> A[i];
    }

    for (j = 0; j < n; j++)
    {
        B[j] = A[x];
        x--;
    }

    for (j = 0; j < n; j++)
    {
        cout << B[j] << " ";
    }


    return 0;
}
