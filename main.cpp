#include<iostream>
#include<stdlib.h>
#include<fstream>

using namespace std;

double Sum(int numbers[], int CountNums);
double Product(int numbers[], int CountNums);
double Average(int numbers[], int CountNums);
double Smallest(int numbers[], int CountNums);

void main()
{
	int i, A[5], number, count = 0;

	fstream dataFile;
	dataFile.open("input.txt", ios::in); //! Read mode opens to transfer data in input.txt file an array

	for (i = 0; i < 5; i++)
	{
		dataFile >> number;
		A[i] = number; //! Data in the file is transferred to the array
		count++;
	}
	dataFile.close();  //! After reading the data in it, the file is closed.


	//! Outputs of the called functions
	cout << "Sum is " << Sum(A, count) << endl;
	cout << "Product is " << Product(A, count) << endl;
	cout << "Average is " << Average(A, count) << endl;
	cout << "Smallest is " << Smallest(A, count) << endl;

	cout << endl << endl;
	system("pause");
}

double Sum(int numbers[], int CountNums)  //! This function returns the sum of numbers of array
{
	double sum = 0;

	for (int i = 1; i < CountNums; i++)
	{
		sum = sum + numbers[i];
	}

	return sum;
}

double Product(int numbers[], int CountNums)  //! This function returns the product of numbers of array
{
	double product = 1;
	for (int i = 1; i < CountNums; i++)
	{
		product = product * numbers[i];
	}

	return product;
}

double Average(int numbers[], int CountNums)  //! This function returns the average of numbers of array
{
	double avg = 0, c = 0;
	for (int i = 1; i < CountNums; i++)
	{
		avg = avg + numbers[i];
		c++;
	}
	avg = avg / c;

	return avg;
}

double Smallest(int numbers[], int CountNums)  //! This function returns the smallest of number of array
{
	int min = numbers[1];   //! The minimum value is assigned to the first element of the array.
	for (int i = 1; i < CountNums; i++)
	{
		if (min > numbers[i])
			min = numbers[i];
	}

	return min;
}